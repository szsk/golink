# GoTo搜索引擎

![GoTologo](static/GoTo_favicon.ico)

>GoTo是孙子烧烤知识分享站旗下的搜索引擎，提供简洁、无广告、安全的搜索界面，为您的隐私保驾护航~

**官网:** [https://go.sunzishaokao.com](https://go.sunzishaokao.com "GoTo搜索")

## 问答：  

### 一、申请收录链接:  
我们提供多种提交链接方式:  

>以下方式需要登陆[GoTo站长平台](https://zhanzhang.sunzishaokao.com "GoTo站长平台")
提交站点信息或获取数据

**1.自动提交（所有页面添加JS代码）:**  

通过用户的访问习惯和页面访问次数自动向GoTo提交站点链接  

**2.Sitemap站点地图提交:**  

在网站根目录创建非索引性的sitemap文件，每天自动向GoTo提交站点链接

**3.API自动提交:**

通过开发等方式，每发布/更新页面都会自动提交到GoTo  

**4.URL手动提交:**  

需要您手动在[GoTo站长平台](https://zhanzhang.sunzishaokao.com "GoTo站长平台")中提交链接

### 二、URL收录规则：

1.您的站点需要正常访问（返回码为200，301，302等）。

2.您的站点未被墙（非代刷站/非法站)。

3.您的站点已经在[GoTo站长平台](https://zhanzhang.sunzishaokao.com "GoTo站长平台")提交网站认证等信息，我们会优先收录。

4.您的站点SEO信息配置正确，则会优先收录。

PS：GoTo爬虫会严格遵从robots.txt文件，您可以在此限制GoTo爬取页面，以保证限制页面不会被收录


## 联系我们：

**1.加入官方微信群:**  

![wxqun](static/wxqun.png)

**2.加入QQ群:**  

![qqqun](static/qqqun.png)  

**3.联系微信客服:**  

[微信客服](https://work.weixin.qq.com/kfid/kfcccbc5c65c7b2a468)  
![wxkf](static/wxkf.png)
